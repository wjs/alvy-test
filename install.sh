#!/bin/bash

set -x

apt-get -y install gcc make python3 valgrind zip unifdef strace libgetopt-complete-perl

echo "success" > '/tmp/rapid_image_complete'
echo "success" > '/tmp/rapid_image_status.txt'
